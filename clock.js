var updateTime = function() {
    /*Obtiene la fecha del equipo y lo divide en partes de fecha y hora(dia,mes,año,hora,minuto,segundo)*/ 
    let currentDate = new Date(),
        hours = currentDate.getHours(),
        minutes = currentDate.getMinutes(), 
        seconds = currentDate.getSeconds(),
        day = currentDate.getDay(), 
        month = currentDate.getMonth(), 
        year = currentDate.getFullYear();
 
    
    /*Busca el elemento con el id "day" y le coloca el valor que obtuvo del sistema*/
    document.getElementById('day').textContent = day;
 /**
  * Array con meses en ingles
  */
    const months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];
 /*Busca el elemento con el id "month" y lo modifica conforme al numero obtenido. Dependiendo ese numero, es el elemento que toma del arreglo de meses*/
    document.getElementById('month').textContent = months[month];
    /*Reemplaza el valor del elemento con el id "year" con el año que tiene el sistema*/
    document.getElementById('year').textContent = year;
 /*Reemplaza el valor del elemento con el id "hours" con la hora actual que tiene el sistema*/
    document.getElementById('hours').textContent = hours;
 /*Para dar formato, si el valor es menor a 10, se le antepone un 0 para que siempre sea un par de numeros*/
    if (minutes < 10) {
        minutes = "0" + minutes
    }
 
    if (seconds < 10) {
        seconds = "0" + seconds
    }
 /*reemplazo de valor en elementos "minutes" y "seconds" con los del sistema*/
    document.getElementById('minutes').textContent = minutes;
    document.getElementById('seconds').textContent = seconds;
};
 /*Ejecucion de la funcion creada anteriormente*/
updateTime();
 /*Con la funcion setInternal se da un intervalo para que cada cierto tiempo,la funcion sea ejecutada*/
setInterval(updateTime, 1000);
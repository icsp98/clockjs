<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
                <div class="date mr-2">
                    <span></span>
                    <span id="month" class="month"></span>,
                    <span id="day" class="day"></span>
                    <span id="year" class="year"></span>
                </div>
                <div class="clock">
                    <span></span>
                    <span id="hours" class="hours"></span> :
                    <span id="minutes" class="minutes"></span> :
                    <span id="seconds" class="seconds"></span>
                </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="clock.js"></script>

</body>
</html>